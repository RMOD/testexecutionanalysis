import urllib
import xmltodict
import re
import pandas as pd
import numpy as np

class BuildsToDataset:
        
    builds = None
    
    def __init__(self):
        self.builds = self.get_builds()
    
    def get_builds(self):
        """
        Returns a list of all the Pharo 9.0 builds by taking the last build from the jenkins xml file and going from 2 to said build.
        It begins at 2 because 0 doesn't exist and 1 is empty.
        """
        file = urllib.request.urlopen("https://ci.inria.fr/pharo-ci-jenkins2/job/Test%20pending%20pull%20request%20and%20branch%20Pipeline/job/Pharo9.0/api/xml") #open file were builds are stored
        data = file.read()
        file.close()
        data = xmltodict.parse(data) #creates a dictionary from the xml
        lastbuild = data['workflowJob']['build'][0]['url'] #extract the last build
        lb_nb = lastbuild.replace("https://ci.inria.fr/pharo-ci-jenkins2/job/Test%20pending%20pull%20request%20and%20branch%20Pipeline/job/Pharo9.0/","") #extracting the number of the build
        nb = int(lb_nb[:-1]) #getting rid of the / at the end and transforming the string to an int
        return ["https://ci.inria.fr/pharo-ci-jenkins2/job/Test%20pending%20pull%20request%20and%20branch%20Pipeline/job/Pharo9.0/"+str(i) for i in range(2,nb+1)]
        
    def get_xml_from_build(self,build):
        """
        build : the url of the build you want to extract
        Returns the list of all the xml url present in the given build 
        """
        try:
            file = urllib.request.urlopen(build+"/api/xml") #open file were tests from classname are stored
            data = file.read()
            file.close()
            data = xmltodict.parse(data) #creates a dictionary from the xml
            data_files = [x['relativePath'] for x in data['workflowRun']['artifact']] #extract all files in the xml
            r = re.compile(".*xml") 
            data_xml = list(filter(r.match, data_files)) #keep only the xml files (getting rid of .zip and other files)
            return data_xml
        except:
            f = open("Update_log.txt", "a")
            f.write("Problem with the build's xml : "+build+"\n") #notify the log that there was a problem
            f.close()
            return []
            
    def get_database_from_xml(self,build,xml):
        """
        build : the url of the build you want to extract
        xml : the local path of the file you want to get
        Returns the dataframe the tests from all the given xml classes of given build 
        """
        xml_rectified = xml.replace(' ', '%20') #change spaces to their url equivalent
        try:
            xml_file = urllib.request.urlopen(build+"/artifact/"+xml_rectified) #open file were tests are stored
            data = xml_file.read()
            xml_file.close()
            data = xmltodict.parse(data) #creates a dictionary from the xml
        except:
            f = open("Update_log.txt", "a")
            f.write("Problem with the package : "+build+"/artifact/"+xml_rectified+"\n")
            f.close()
        try:
            df = pd.DataFrame(data['testsuite']['testcase']) #create a dataframe
            df['@package'] = pd.Series([ data['testsuite']['@name'] for i in range(df.shape[0])])
        except:
            df = pd.DataFrame()
        return df
        
    def get_all_database_from_build(self,build):
        """
        build : the url of the build you want to extract
        Returns the complete dataframe with all the tests from all the builds
        """
        data_xml = self.get_xml_from_build(build)
        df = pd.DataFrame()
        for xml in data_xml:
            df = df.append(self.get_database_from_xml(build,xml))
        return df   
        
    def get_error_database_from_build(self,build):
        """
        build : the url of the build you want to extract<br>
        Return the dataframe of all the tests which had an error/failure in given build.
        If there was none, then it returns an empty dataframe.
        """
        data_xml = self.get_xml_from_build(build)
        df = pd.DataFrame()
        for xml in data_xml:
            df = df.append(self.get_database_from_xml(build,xml))
        #checking for the presence of errors or failures in the dataframe
        try:
            df['error']
        except:
            try:
                df['failure']
            except:
                return pd.DataFrame()
        return df.dropna() #returning only the rows were there was an error or failure
        
    def get_errors_df(self):
        """
        Returns the dataframe containing all the errors/failures present in jenkin's database of builds.
        """
        # f = open("Update_log.txt", "a")
        # f.write("Beginning the creation of error build\n")
        # f.close()
        # df = self.get_error_database_from_build(self.builds[10])
        # if(df.shape[0]>0):
            # df['build'] = [2 for i in range(df.shape[0])]
        # for i in range(11, len(self.builds)):
            # f = open("Update_log.txt", "a")
            # try:
                # newdf = self.get_error_database_from_build(self.builds[i])
                # newdf['build'] = [i+2 for j in range(newdf.shape[0])]
                # df = df.append(newdf)
                # f.write("Build "+str(i+2)+" integrated\nsize = "+str(df.shape[0])+"x"+str(df.shape[1])+"\n")
            # except KeyError:
                # f.write("Build "+str(i+2)+" returned nothing\n")
            # if(i%10==0):
                # df.to_csv("Error_dataframe.csv")
                # f.write("DataFrame saved\n")
            # f.close()
        # df.to_csv("Error_dataframe.csv")
        return dself.get_errors_df_from_to(2,len(self.builds)+1)

    def get_errors_df_from_to(self, f, t, csv="Error_dataframe.csv", from_file=False):
        """
        f : the number of the build you want to begin at
        t : the number of the build you want to end at
        f < t
        Returns the dataframe containing the errors/failures present in jenkin's database of builds from one build to another.
        """
        file = open("Update_log.txt", "a")
        file.write("Adding errors from build "+str(f)+" to "+str(t)+"\n")
        file.close()
        if from_file:
            df = pd.read_csv(csv)
            d = f-2
        else:
            df = self.get_error_database_from_build(self.builds[f-2])
            newdf['build'] = [f+2 for j in range(df.shape[0])]
            d=f-1 
        for i in range(d, t-1):
            file = open("Update_log.txt", "a")
            try:
                newdf = self.get_error_database_from_build(self.builds[i])
                newdf['build'] = [i+2 for j in range(newdf.shape[0])]
                df = df.append(newdf)
                file.write("Build "+str(i+2)+" integrated\nsize = "+str(df.shape[0])+"x"+str(df.shape[1])+"\n")
            except KeyError:
                file.write("Build "+str(i+2)+" returned nothing\n")
            if(i%10==0):
                df.to_csv("Error_dataframe.csv")
                file.write("DataFrame saved at build "+str(i+2)+"\n")
            file.close()
        df.to_csv("Error_dataframe.csv")
        return df
        
    def append(self, df1, df2, name=None):
        """
        df1 : the main pandas dataframe
        df2 : the pandas dataframe you want to append 
        name : string of the path of the file you want to save the dataframe on (optional)
        Returns a dataframe with the rows of df2 added to the ones of df1.
        The variable name is there if you want to save the datafreame on a file.
        """
        f = open("Update_log.txt", "a")
        df = df1
        try:
            df = df.append(df2)
            f.write("size = "+str(df.shape[0])+"x"+str(df.shape[1])+"\n")
            if name != None:
                df.to_csv(name)
                f.write("DataFrame saved\n")
        except KeyError:
            f.write("Build returned nothing\n")
        f.close()
        return df
    
    def append_errordf(self, df2):
        """
        df2 : the pandas dataframe you want to append
        Returns a dataframe with the rows of df2 added to the ones of Error_dataframe.csv.
        The new dataframe is stored in Error_dataframe.csv
        """
        return self.append(pd.read_csv("Error_dataframe.csv"), df2, "Error_dataframe.csv")
        
    def get_full_df(self):
        """
        Returns the dataframe of the last build with a time that is a mean of all times from all builds.
        """
        f = open("Update_log.txt", "a")
        f.write("Beginning the creation of Build_"+str(len(self.builds)+1)+".csv\n")
        f.close()
        return self.full_df_from(len(self.builds)+1, "Build_"+str(len(self.builds)+1)+".csv")
    
    def full_df_from(self, number, csv=None):
        """
        number : the number of the build you want the means of
        csv : string of the path of the file you want to save the dataframe on (optional)
        Returns the dataframe of the given build with a time that is a mean of all times from all previous builds.
        """
        return self.full_df_from_to(number,2, csv)
        
    def full_df_from_to(self,f,t, csv=None, from_file=False):
        """
        f : the number of the build you want the means of
        t : the number of the build you want to stop at
        f > t
        csv : string of the path of the file you want to save the dataframe on (optional)
        Returns the dataframe of the given build with a time that is a mean of all times from previous builds, stopping at the given build.
        """
        file = open("Update_log.txt", "a")
        if(csv != None):
            file.write("Constructing "+csv+" for "+str(f)+" to "+str(t)+"\n")
        else:
            file.write("Constructing dataframe for "+str(f)+" to "+str(t)+"\n")
        file.close()
        if from_file:
            df = pd.read_csv(csv)
            d = f-2
        else:
            build = self.builds[f-2]
            df = self.get_usable_df(build)
            d=f-3
        for i in range(d, t-3,-1):
            file = open("Update_log.txt", "a")
            try:
                newdf = self.get_usable_df(self.builds[i])
                df = self.merge_left(df,newdf)
                file.write("Build "+str(i+2)+" integrated\nsize = "+str(df.shape[0])+"x"+str(df.shape[1])+"\n")
            except KeyError:
                file.write("Build "+str(i+2)+" returned nothing\n")
            if(i%10==0) and (csv != None):
                df.to_csv(csv)
                file.write("DataFrame saved\n")
            file.close()
        if(csv != None):
            df.to_csv(csv)
        return df

    def merge_left(self, df1, df2,csv= None):
        """
        df1 : the left pandas dataframe
        df2 : the right pandas dataframe 
        csv : string of the path of the file you want to save the dataframe on (optional)
        Returns a dataframe where the mean time has been calculated and the number of presence in builds incremented
        """
        df1 = df1.merge(df2, how='left', on=['@package', '@classname', '@name'], suffixes=('', '_n'))
        df1["nb_builds_n"] = df1["nb_builds_n"].fillna(0)
        df1["@time_n"] = df1["@time_n"].fillna(0)
        df1['@time'] = (df1["@time"]*df1["nb_builds"]+df1["@time_n"]*df1["nb_builds_n"])/(df1["nb_builds"]+df1["nb_builds_n"])
        df1['@time_min'] = df1[["@time_min", "@time_min_n"]].min(axis=1)
        df1['@time_max'] = df1[["@time_max", "@time_max_n"]].max(axis=1)
        df1["nb_builds"] = df1["nb_builds"]+df1["nb_builds_n"]
        df1 = df1.drop(["@time_n", "nb_builds_n", "@time_max_n", "@time_min_n"],axis=1)
        if(csv != None):
            df1.to_csv(csv)
        return df1
        
    def get_usable_df(self, build):
        """
        build : the url of the build you want to extract<br>
        Return the dataframe prepared to be used for mean time dataframe
        """
        newdf = self.get_all_database_from_build(build)
        newdf["@time"] = pd.to_numeric(newdf["@time"])
        newdf["@time_min"] = newdf["@time"]
        newdf["@time_max"] = newdf["@time"]
        newdf['nb_builds'] = np.ones(newdf.shape[0])
        newdf = newdf.dropna(axis=1)
        newdf = newdf.sort_values(by='@time')
        newdf = newdf.drop_duplicates(['@classname','@name'], keep='last')
        return newdf
  
if __name__ == "__main__":
    bd = BuildsToDataset()
    bd.get_errors_df_from_to(373,1248,from_file=True)
    
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        