File BuildsToDataset.py:

The goal of this file is to offer a class that allows for easy recuperation of the pharo builds. <br>
It accounts for 2 types of builds : - the failures and errors build <br>
                                    - the mean with previous builds <br>
The first one goes through each build, selects the rows with errors/failures and appends them into a single dataframe. <br>
The goal is to extract informations on the time, the reason and the frequency of the error. <br>
The second goes through each previous build of the selected one to make the mean time of test execution. <br>
The goal is to see the gobal length of each test run, the difference between the last iteration and the mean and the number of builds it went through. <br><br><br>
Requirements : <br>
This class needs the following packages : pandas, numpy, urllib, re and xmltodict <br>
To install this packages under python3, you need pip which you can get with : sudo apt install python3-pip <br>
Then to install a package, you do the following : pip3 install \< name of the package\> <br><br>

The class: <br>

get_builds(self): <br>
    Returns a list of all the Pharo 9.0 builds by taking the last build from the jenkins xml file and going from 2 to said build. <br>
    It begins at 2 because 0 doesn't exist and 1 is empty. <br>

get_xml_from_build(self,build):<br>
    build : the url of the build you want to extract <br>
    Returns the list of all the xml url present in the given build<br>

get_database_from_xml(self,build,xml): <br>
    build : the url of the build you want to extract<br>
    xml : the local path of the file you want to get<br>
    Returns the dataframe the tests from all the given xml classes of given build<br>

get_all_database_from_build(self,build):<br>
    build : the url of the build you want to extract<br>
    Returns the complete dataframe with all the tests from all the builds<br>

get_error_database_from_build(self,build):<br>
    build : the url of the build you want to extract<br>
    Return the dataframe of all the tests which had an error/failure in given build.<br>
    If there was none, then it returns an empty dataframe.<br>

get_errors_df(self):<br>
    Returns the dataframe containing all the errors/failures present in jenkin's database of builds.<br>

get_errors_df_from_to(self, f, t):<br>
    f : the number of the build you want to begin at <br>
    t : the number of the build you want to end at <br>
    f < t <br>
    Returns the dataframe containing the errors/failures present in jenkin's database of builds from one build to another.

append(self, df1, df2, name=None):<br>
    df1 : the main pandas dataframe <br>
    df2 : the pandas dataframe you want to append <br>
    name : string of the path of the file you want to save the dataframe on (optional) <br>
    Returns a dataframe with the rows of df2 added to the ones of df1.<br>
    The variable name is there if you want to save the datafreame on a file.<br>

append_errordf(self, df2): <br>
    df2 : the pandas dataframe you want to append <br>
    Returns a dataframe with the rows of df2 added to the ones of Error_dataframe.csv. <br>
    The new dataframe is then stored in Error_dataframe.csv <br>

get_full_df(self): <br>
    Returns the dataframe of the last build with a time that is a mean of all times from all builds. <br>

full_df_from(self, number, csv=None):<br>
    number : the number of the build you want the means of<br>
    csv : string of the path of the file you want to save the dataframe on (optional)<br>
    Returns the dataframe of the given build with a time that is a mean of all times from all previous builds.<br>

full_df_from_to(self,f,t, csv=None):<br>
    f : the number of the build you want the means of<br>
    t : the number of the build you want to stop at<br>
    f > t<br>
    csv : string of the path of the file you want to save the dataframe on (optional)<br>
    Returns the dataframe of the given build with a time that is a mean of all times from previous builds, stopping at the given build.<br>

merge_left(self, df1, df2, csv= None):<br>
    df1 : the left pandas dataframe<br>
    df2 : the right pandas dataframe <br>
    csv : string of the path of the file you want to save the dataframe on (optional)<br>
    Returns a dataframe where the mean time has been calculated and the number of presence in builds incremented<br>

get_usable_df(self, build):<br>
    build : the url of the build you want to extract<br>
    Return the dataframe prepared to be used for mean time dataframe.<br>


