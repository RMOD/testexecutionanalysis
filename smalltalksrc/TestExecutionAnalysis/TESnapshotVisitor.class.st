Class {
	#name : #TESnapshotVisitor,
	#superclass : #Object,
	#instVars : [
		'extractor',
		'currentSnapshot',
		'currentPackage'
	],
	#category : #TestExecutionAnalysis
}

{ #category : #accessing }
TESnapshotVisitor >> extractor: anExtractor [ 
	
	extractor := anExtractor
]

{ #category : #visiting }
TESnapshotVisitor >> visit: aMCSnapshot ofPackage: aPackage [

	currentPackage := aPackage.	
	currentSnapshot := aMCSnapshot.
	
	currentSnapshot definitions do: [ :e | e accept: self ]
]

{ #category : #visiting }
TESnapshotVisitor >> visitClassDefinition: aMCClassDefinition [ 
	
	
]

{ #category : #visiting }
TESnapshotVisitor >> visitClassTraitDefinition: aMCClassTraitDefinition [ 

	
]

{ #category : #visiting }
TESnapshotVisitor >> visitMetaclassDefinition: aMCClassDefinition [ 

	
]

{ #category : #visiting }
TESnapshotVisitor >> visitMethodDefinition: aMethod [ 
	
	| aClass |
	
	(aMethod selector beginsWith: 'test')
		ifFalse: [ ^ self ].
	
	aClass := currentSnapshot classDefinitionNamed: aMethod className 
		ifAbsent: [ ^ extractor processExtensionTest: aMethod inPackage: currentPackage. ].
		
	extractor processTest: aMethod ofClass: aClass inPackage: currentPackage.
]

{ #category : #visiting }
TESnapshotVisitor >> visitOrganizationDefinition: aMCOrganizationDefinition [ 

	"I Do nothing for them"
]

{ #category : #visiting }
TESnapshotVisitor >> visitTraitDefinition: aMCTraitDefinition [ 

	
]
