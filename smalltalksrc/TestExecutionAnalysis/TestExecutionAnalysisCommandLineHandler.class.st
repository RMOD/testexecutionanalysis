"
This is the command line generates a file with the information from the Pharo repository for a given set of build

Usage: testDataExtractor <outputFile> <fromBuild> <toBuild>
	 <outputFile>      	The path of the CSV output file
	 <fromBuild>     	 	The initial build to process
	 <toBuild>      		The last build to process
	

"
Class {
	#name : #TestExecutionAnalysisCommandLineHandler,
	#superclass : #CommandLineHandler,
	#category : #TestExecutionAnalysis
}

{ #category : #accessing }
TestExecutionAnalysisCommandLineHandler class >> commandName [
	
	^ 'testDataExtractor'
]

{ #category : #accessing }
TestExecutionAnalysisCommandLineHandler class >> description [ 

	^ 'Extracts information from a Pharo github repository for a give set of Jenkins builds'
]

{ #category : #activation }
TestExecutionAnalysisCommandLineHandler >> activate [
	self activateHelp ifTrue: [ ^ self ].

	TEPerBuildRunner new
		fileName: self outputFile;
		writeHeader;
		runFrom: self from to: self to;
		close.

	self exitSuccess
]

{ #category : #activation }
TestExecutionAnalysisCommandLineHandler >> from [
	
	^ (commandLine arguments at: 2) asInteger

]

{ #category : #activation }
TestExecutionAnalysisCommandLineHandler >> outputFile [
	
	^ commandLine arguments at: 1

]

{ #category : #activation }
TestExecutionAnalysisCommandLineHandler >> to [
	
	^ (commandLine arguments at: 3) asInteger

]
