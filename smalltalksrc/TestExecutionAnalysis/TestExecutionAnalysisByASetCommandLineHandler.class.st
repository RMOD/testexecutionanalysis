Class {
	#name : #TestExecutionAnalysisByASetCommandLineHandler,
	#superclass : #CommandLineHandler,
	#category : #TestExecutionAnalysis
}

{ #category : #accessing }
TestExecutionAnalysisByASetCommandLineHandler class >> commandName [
	
	^ 'testDataExtractorFromFiles'
]

{ #category : #accessing }
TestExecutionAnalysisByASetCommandLineHandler class >> description [ 

	^ 'Extracts information from a Pharo github repository for a give set of Jenkins builds, taking the numbers from an input file'
]

{ #category : #activation }
TestExecutionAnalysisByASetCommandLineHandler >> activate [
	self activateHelp ifTrue: [ ^ self ].

	TEPerBuildRunner new
		fileName: self outputFile;
		writeHeader;
		runForBuilds: self buildsToRun;
		close.

	self exitSuccess
]

{ #category : #activation }
TestExecutionAnalysisByASetCommandLineHandler >> buildsToRun [
	
	| reader collection |

	self inputFile asFileReference readStreamDo: [ :aStream |
		reader := NeoCSVReader on: aStream.
		collection := reader skipHeader; upToEnd.
		^(collection collect: [ :a | (a at: 1) asNumber]) asSet asOrderedCollection 
		sort:[:a :b | a< b]]

]

{ #category : #activation }
TestExecutionAnalysisByASetCommandLineHandler >> inputFile [
	
	^ commandLine arguments at: 2
]

{ #category : #activation }
TestExecutionAnalysisByASetCommandLineHandler >> outputFile [
	
	^ commandLine arguments at: 1

]
