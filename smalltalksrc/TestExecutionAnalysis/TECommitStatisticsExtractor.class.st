Class {
	#name : #TECommitStatisticsExtractor,
	#superclass : #Object,
	#instVars : [
		'commit',
		'csvWriter',
		'buildNumber'
	],
	#category : #TestExecutionAnalysis
}

{ #category : #events }
TECommitStatisticsExtractor class >> onCommit: anIcebergCommit onWriter: aCSVWriter forBuildNumber: aBuildNumber [

	^ self new
		commit: anIcebergCommit;
		csvWriter: aCSVWriter;
		buildNumber: aBuildNumber;
		yourself
]

{ #category : #writing }
TECommitStatisticsExtractor class >> writeHeader: csvWriter [

	"Add Header"
	csvWriter 
		nextPut: {
			'BuildNumber'.
			'CommitId'. 
			'PackageName'.
			'ClassName'.
			'selector'.
			'sourceCode'.
			'numberOfGlobalsUsed'.			
			'numberOfClassesUsed'.
			'numberOfMessagesUsed'.
			'numberOfLocalVariables'.
			'lines' }.

]

{ #category : #accessing }
TECommitStatisticsExtractor >> buildNumber: anInteger [ 
	buildNumber := anInteger
]

{ #category : #accessing }
TECommitStatisticsExtractor >> commit: anIceGitCommit [ 
	commit := anIceGitCommit
]

{ #category : #accessing }
TECommitStatisticsExtractor >> csvWriter: aCSVWriter [

	csvWriter := aCSVWriter
]

{ #category : #'private - operations' }
TECommitStatisticsExtractor >> doExecute: anIcePackage [ 
	
	| snapshots |
	
	snapshots := commit mcPackageSnapshotsOfPackages: { anIcePackage }.
	snapshots do: [ :e | self processSnapshot: e ofPackage: anIcePackage ]
]

{ #category : #operations }
TECommitStatisticsExtractor >> execute [
				
	commit 
		packages do: [ :aPackage | self doExecute: aPackage ] 
		displayingProgress: [ :e | e name ].
]

{ #category : #stats }
TECommitStatisticsExtractor >> numberOfClassesUsed: aRBMethodNode [ 
	
	| result | 
	
	result := Set new.
	
	aRBMethodNode allChildren select: [ :aChild | aChild isVariable and: [ aChild name first isUppercase ] ] 
		thenDo: [ :aVariable | | name |
			name := aVariable name.
			Smalltalk at: name asSymbol 
				ifPresent: [ :aGlobal | aGlobal isClass ifTrue: [ result add: name ] ]  
				ifAbsent: [ ]   ].
			
	^ result size
]

{ #category : #stats }
TECommitStatisticsExtractor >> numberOfGlobalsUsed: aRBMethodNode [ 
	
	| result | 
	
	result := Set new.
	
	aRBMethodNode allChildren select: [ :aChild | aChild isVariable and: [ aChild name first isUppercase ] ] 
		thenDo: [ :aVariable | | name |
			name := aVariable name.
			Smalltalk at: name asSymbol 
				ifPresent: [ :aGlobal | aGlobal isClass ifFalse: [ result add: name ] ]  
				ifAbsent: [ result add: name ]   ].
			
	^ result size
]

{ #category : #stats }
TECommitStatisticsExtractor >> numberOfLocalVariables: aRBMethodNode [

	^ aRBMethodNode body temporaries size
]

{ #category : #stats }
TECommitStatisticsExtractor >> numberOfMessagesUsed: aRBMethodNode [ 
	
	| result | 
	
	result := Set new.
	
	aRBMethodNode allChildren select: [ :aChild | aChild isMessage ] 
		thenDo: [ :aMessageSend | result add: aMessageSend selector   ].
			
	^ result size
]

{ #category : #processing }
TECommitStatisticsExtractor >> processExtensionTest: aMethod inPackage: aPackage [ 

	| ast |
	
	[ast := RBParser parseMethod: aMethod source.
	ast doSemanticAnalysis] on: OCUndeclaredVariableWarning do: [].
	
	csvWriter 
		nextPut: {
			buildNumber.
			commit id. 
			aPackage name.
			aMethod className.
			aMethod selector.
			aMethod source.
			self numberOfGlobalsUsed: ast.			
			self numberOfClassesUsed: ast.
			self numberOfMessagesUsed: ast.
			self numberOfLocalVariables: ast.
			aMethod source lines size }.
]

{ #category : #'private - operations' }
TECommitStatisticsExtractor >> processSnapshot: aMCSnapshot [ 

	| visitor |

	visitor := TESnapshotVisitor new.
	visitor extractor: self.
	visitor visit: aMCSnapshot
]

{ #category : #'private - operations' }
TECommitStatisticsExtractor >> processSnapshot: aMCSnapshot ofPackage: aPackage [

	| visitor |

	visitor := TESnapshotVisitor new.
	visitor extractor: self.
	visitor visit: aMCSnapshot ofPackage: aPackage
]

{ #category : #processing }
TECommitStatisticsExtractor >> processTest: aMethod ofClass: aClass inPackage: aPackage [

	| ast |
	
	[ast := RBParser parseMethod: aMethod source.
	ast doSemanticAnalysis] on: OCUndeclaredVariableWarning do: [].
	
	csvWriter 
		nextPut: { 
			buildNumber.
			commit id. 
			aPackage name.
			aClass className.
			aMethod selector.
			aMethod source.
			self numberOfGlobalsUsed: ast.			
			self numberOfClassesUsed: ast.
			self numberOfMessagesUsed: ast.
			self numberOfLocalVariables: ast.
			aMethod source lines size }.
]
