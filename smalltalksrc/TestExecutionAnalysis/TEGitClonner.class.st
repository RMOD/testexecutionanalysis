Class {
	#name : #TEGitClonner,
	#superclass : #Object,
	#category : #TestExecutionAnalysis
}

{ #category : #operations }
TEGitClonner >> cloneOrLocate [

	| builder |
	
	'Cloning or locating the Pharo repository' traceCr.
	
	[ 
	builder := IceRepositoryCreator new
		           url: 'https://github.com/pharo-project/pharo.git';
		           subdirectory: 'src';
		           ensureProjectFile;
		           yourself.
	^ builder createRepository ]
		on: IceDuplicatedRepository
		do: [ 
			builder := IceRepositoryCreator new
				           location:
					           IceLibgitRepository repositoriesLocation
					           / 'pharo-project' / 'pharo';
				           subdirectory: 'src';
				           ensureProjectFile;
				           yourself.
			^ builder createRepository ]
]

{ #category : #operations }
TEGitClonner >> ensureRepository: aBlock [

	| repository |
	repository := self cloneOrLocate.
	repository fetch.

	^ aBlock value: repository
]
