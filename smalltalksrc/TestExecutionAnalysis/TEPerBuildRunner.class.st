Class {
	#name : #TEPerBuildRunner,
	#superclass : #Object,
	#instVars : [
		'fileName',
		'writeStream',
		'csvStream'
	],
	#category : #TestExecutionAnalysis
}

{ #category : #operations }
TEPerBuildRunner >> close [

	csvStream close.
]

{ #category : #private }
TEPerBuildRunner >> fetchCommitIdFor: anInteger [ 

	| contents start end |
	contents := (ZnEasy get: 'https://ci.inria.fr/pharo-ci-jenkins2/job/Test%20pending%20pull%20request%20and%20branch%20Pipeline/job/Pharo9.0/', anInteger asString , '/api/xml') contents.

	start := (contents indexOfSubCollection: '<SHA1>') + 6.
	end := (contents indexOfSubCollection: '</SHA1>') - 1.

	^ contents copyFrom: start to: end.

]

{ #category : #accessing }
TEPerBuildRunner >> fileName: aString [ 
	
	fileName := aString asFileReference.
	fileName parent ensureCreateDirectory.
	
	writeStream := fileName asFileReference writeStream.
	csvStream := NeoCSVWriter on: writeStream
]

{ #category : #operations }
TEPerBuildRunner >> run: aBuildNumber on: repository [
	
	| commitId commit extractor |
	commitId := self fetchCommitIdFor: aBuildNumber.
	
	commit := repository lookupCommit: commitId.
	extractor := TECommitStatisticsExtractor 
		onCommit: commit onWriter: csvStream forBuildNumber: aBuildNumber.
	extractor execute .

]

{ #category : #operations }
TEPerBuildRunner >> runForBuilds: aCollection [

	TEGitClonner new ensureRepository: [ :repository | 
		aCollection
			do: [ :aBuildNumber | 
				('Running build number ' , aBuildNumber asString) traceCr.
				self run: aBuildNumber on: repository ]
			displayingProgress: [ :aBuildNumber | 'Processing Build ' , aBuildNumber asString ] ]
]

{ #category : #operations }
TEPerBuildRunner >> runFrom: from to: to [

	TEGitClonner new ensureRepository: [ :repository | 
		(from to: to)
			do: [ :aBuildNumber | 
				('Running build number ' , aBuildNumber asString , '/' , to asString) traceCr.
				self run: aBuildNumber on: repository ]
			displayingProgress: [ :aBuildNumber | 'Processing Build ' , aBuildNumber asString ] ]
]

{ #category : #operations }
TEPerBuildRunner >> writeHeader [
	
	TECommitStatisticsExtractor writeHeader: csvStream
]
