Class {
	#name : #BaselineOfTestExecutionAnalysis,
	#superclass : #BaselineOf,
	#category : #BaselineOfTestExecutionAnalysis
}

{ #category : #baselines }
BaselineOfTestExecutionAnalysis >> baseline: spec [
	<baseline>
	spec for: #common do: [ spec
		baseline: 'NeoCSV' with: [ spec repository: 'github://svenvc/NeoCSV/repository' ];

		package: 'TestExecutionAnalysis' with: [ spec requires: #('NeoCSV') ];

		group: 'default' with: #(core);
		group: 'core' with: #('TestExecutionAnalysis') ]
]
