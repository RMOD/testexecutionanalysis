# A project to extract information from the execution of the tests in Jenkins

TODO

# Extracting information from the Pharo GIT repository

To reproduce it execute the following commands, it should be done inside a clone of this repository.
It creates a ``build`` directory, with the files modified and created.

## Preparation
```
mkdir build
cd build
wget -O - get.pharo.org/64/90+vmHeadlessLatest | bash
./pharo Pharo.image metacello install "gitlocal://.." BaselineOfTestExecutionAnalysis
```

## Execution for a range of builds

```
./pharo Pharo.image testDataExtractor <outputFile> <initialCommit> <finalCommit>
```

### Example

```
./pharo Pharo.image testDataExtractor test.csv 1175 1178
```

## Execution for a set of build numbers in a CSV files

It uses an inputFile that is a CSV with a single column with the number of the builds to process.
The process will remove duplicates and execute it in ascending order.

```
./pharo Pharo.image testDataExtractorFromFiles <outputFile> <inputFile>
```

### Example

```
./pharo Pharo.image testDataExtractorFromFiles builds.csv runsWithErrors.csv
```
